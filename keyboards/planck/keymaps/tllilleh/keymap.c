/* Copyright 2015-2021 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

enum planck_keycodes {
  SONG_1 = SAFE_RANGE,
  SONG_2,
  SONG_3,
  SONG_4,
  SONG_5,
  SONG_6
};

#define CTL_ESC LCTL_T(KC_ESCAPE)
#define L1_ENT LT(1, KC_ENTER)
#define L2_SPC LT(2, KC_SPC)
#define L3 MO(3)
#define L4 MO(4)
#define RSFT_ENT RSFT_T(KC_ENTER)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [0] = LAYOUT_planck_grid( \
  //,-----------------------------------------------------------------------------------------------------------.
       KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,   KC_P,  KC_BSPC,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      CTL_ESC,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN,KC_QUOTE,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      KC_LSFT,    KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M, KC_COMM,  KC_DOT, KC_SLSH,RSFT_ENT,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
       SONG_1,  SONG_2,  SONG_3, KC_LALT, KC_LGUI,  L1_ENT,  L2_SPC,      L4, KC_RALT,  SONG_4,  SONG_5, SONG_6
  //`-----------------------------------------------------------------------------------------------------------'
  ),

  [1] = LAYOUT_planck_grid( \
  //,----------------------------------------------------------------------------------------------------------.
       KC_TAB, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PLUS,    KC_7,    KC_8,    KC_9, KC_ASTR,  KC_DEL,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      CTL_ESC, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,KC_EQUAL,    KC_4,    KC_5,    KC_6, KC_BSLS, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      KC_LSFT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, C(KC_B), KC_AMPR,    KC_1,    KC_2,    KC_3, KC_MINS,RSFT_ENT,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, KC_LALT, KC_LGUI,  L1_ENT,      L3,    KC_0, KC_RALT, XXXXXXX, XXXXXXX, XXXXXXX
  //`-----------------------------------------------------------------------------------------------------------'
    ),

  [2] = LAYOUT_planck_grid( \
  //,-----------------------------------------------------------------------------------------------------------.
       KC_TAB, KC_EXLM,   KC_AT, KC_LCBR, KC_RCBR, KC_PIPE, XXXXXXX, KC_PGUP, KC_HOME, XXXXXXX, XXXXXXX,  KC_DEL,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      CTL_ESC, KC_HASH,  KC_DLR, KC_LPRN, KC_RPRN,  KC_GRV, KC_LEFT, KC_DOWN,   KC_UP,KC_RIGHT, XXXXXXX, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      KC_LSFT, KC_PERC, KC_CIRC, KC_LBRC, KC_RBRC, KC_TILD, XXXXXXX, KC_PGDN,  KC_END, XXXXXXX, XXXXXXX,RSFT_ENT,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, KC_LALT, KC_LGUI,      L3,  L2_SPC, XXXXXXX, KC_RALT, XXXXXXX, XXXXXXX, XXXXXXX
  //`-----------------------------------------------------------------------------------------------------------'
  ),

  [3] = LAYOUT_planck_grid( \
  //,-----------------------------------------------------------------------------------------------------------.
      QK_BOOT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   KC_F7,   KC_F8,   KC_F9, XXXXXXX, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   KC_F4,   KC_F5,   KC_F6, XXXXXXX, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      KC_LSFT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   KC_F1,   KC_F2,   KC_F3, XXXXXXX,RSFT_ENT,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, KC_LALT, KC_LGUI,      L3,      L3, XXXXXXX, KC_RALT, XXXXXXX, XXXXXXX, XXXXXXX
  //`-----------------------------------------------------------------------------------------------------------'
    ),

  [4] = LAYOUT_planck_grid( \
  //,-----------------------------------------------------------------------------------------------------------.
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, G(KC_7), G(KC_8), G(KC_9), XXXXXXX, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, G(KC_4), G(KC_5), G(KC_6), XXXXXXX, XXXXXXX,\
  //|--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
      KC_LSFT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, G(KC_1), G(KC_2), G(KC_3), XXXXXXX,RSFT_ENT,\
  //|--------+--------+--------+--------+--------+--------+-------+--------+---------+--------+--------+--------|
      XXXXXXX, XXXXXXX, XXXXXXX, KC_LALT, KC_LGUI,G(KC_ENTER),L2_SPC,    L4,  KC_RALT, XXXXXXX, XXXXXXX, XXXXXXX
  //`-----------------------------------------------------------------------------------------------------------'
    )
};


#ifdef AUDIO_ENABLE

#undef COIN_SOUND
#define COIN_SOUND \
    E__NOTE(_A5  ), \
    HD_NOTE(_E6  ),

#undef ONE_UP_SOUND
#define ONE_UP_SOUND \
    Q__NOTE(_E6  ),  \
    Q__NOTE(_G6  ),  \
    Q__NOTE(_E7  ),  \
    Q__NOTE(_C7  ),  \
    Q__NOTE(_D7  ),  \
    Q__NOTE(_G7  ),

#undef ZELDA_PUZZLE
#define ZELDA_PUZZLE \
    Q__NOTE(_G5), \
    Q__NOTE(_FS5),\
    Q__NOTE(_DS5),\
    Q__NOTE(_A4), \
    Q__NOTE(_GS4),\
    Q__NOTE(_E5), \
    Q__NOTE(_GS5),\
    HD_NOTE(_C6),

#undef ZELDA_TREASURE
#define ZELDA_TREASURE \
    Q__NOTE(_A4), \
    Q__NOTE(_AS4),\
    Q__NOTE(_B4), \
    HD_NOTE(_C5), \

#undef MARIO_THEME
#define MARIO_THEME \
    Q__NOTE(_E5), \
    H__NOTE(_E5), \
    H__NOTE(_E5), \
    Q__NOTE(_C5), \
    H__NOTE(_E5), \
    W__NOTE(_G5), \
    Q__NOTE(_G4),

#undef MARIO_GAMEOVER
#define MARIO_GAMEOVER \
    HD_NOTE(_C5 ), \
    HD_NOTE(_G4 ), \
    H__NOTE(_E4 ), \
    H__NOTE(_A4 ), \
    H__NOTE(_B4 ), \
    H__NOTE(_A4 ), \
    H__NOTE(_AF4), \
    H__NOTE(_BF4), \
    H__NOTE(_AF4), \
    WD_NOTE(_G4 ),

#undef MARIO_MUSHROOM
#define MARIO_MUSHROOM \
    S__NOTE(_C5 ), \
    S__NOTE(_G4 ), \
    S__NOTE(_C5 ), \
    S__NOTE(_E5 ), \
    S__NOTE(_G5 ), \
    S__NOTE(_C6 ), \
    S__NOTE(_G5 ), \
    S__NOTE(_GS4), \
    S__NOTE(_C5 ), \
    S__NOTE(_DS5), \
    S__NOTE(_GS5), \
    S__NOTE(_DS5), \
    S__NOTE(_GS5), \
    S__NOTE(_C6 ), \
    S__NOTE(_DS6), \
    S__NOTE(_GS6), \
    S__NOTE(_DS6), \
    S__NOTE(_AS4), \
    S__NOTE(_D5 ), \
    S__NOTE(_F5 ), \
    S__NOTE(_AS5), \
    S__NOTE(_D6 ), \
    S__NOTE(_F6 ), \
    S__NOTE(_AS6), \
    S__NOTE(_F6 )

  float song_1[][2] = SONG(ZELDA_PUZZLE);
  float song_2[][2] = SONG(ZELDA_TREASURE);
  float song_3[][2] = SONG(ODE_TO_JOY); //SONG(NOCTURNE_OP_9_NO_1); //SONG(COIN_SOUND);
  float song_4[][2] = SONG(MARIO_THEME);
  float song_5[][2] = SONG(MARIO_MUSHROOM);
  float song_6[][2] = SONG(ONE_UP_SOUND);
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case SONG_1:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_1);
#endif
            }
            return false;
            break;

        case SONG_2:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_2);
#endif
            }
            return false;
            break;

        case SONG_3:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_3);
#endif
            }
            return false;
            break;

        case SONG_4:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_4);
#endif
            }
            return false;
            break;

        case SONG_5:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_5);
#endif
            }
            return false;
            break;

        case SONG_6:
            if (record->event.pressed) {
#ifdef AUDIO_ENABLE
                stop_all_notes();
                PLAY_SONG(song_6);
#endif
            }
            return false;
            break;

    }
    return true;
}
