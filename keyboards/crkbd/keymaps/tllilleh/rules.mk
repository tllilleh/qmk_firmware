# Enable Link Time Optimizations
LTO_ENABLE = yes

BOOTLOADER = atmel-dfu  # Elite-C

SRC +=  ./lib/logo_reader.c \
		heatmap.c

OLED_ENABLE = yes
OLED_DRIVER = ssd1306
