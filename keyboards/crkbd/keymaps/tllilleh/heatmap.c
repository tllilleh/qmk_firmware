#include QMK_KEYBOARD_H

#define ROWS 4
#define COLS 16
static int last_row;
static int last_col;
static uint8_t heatmap_count_row[ROWS][COLS] = {};
static uint8_t heatmap_count_max = 0;
static char heatmap_str_row[ROWS][COLS] = {};

static void convert_row_col(int r_in, int c_in, int * r_out, int * c_out)
{
    int row = r_in;
    int col = c_in;

    if (row == 3 || row == 7)
    {
        col += 1;
    }

    if (row > 3)
    {
        row -= 4;
        col = 14 - col;
    }

    if (r_out)
        *r_out = row;

    if (c_out)
        *c_out = col;
}

void set_heatmap(uint16_t keycode, keyrecord_t *record)
{
    last_row = record->event.key.row;
    last_col = record->event.key.col;

    int row;
    int col;
    convert_row_col(last_row, last_col, &row, &col);

    if (heatmap_count_row[row][col] == 255)
    {
        heatmap_count_max /= 2;
        for (row = 0; row < ROWS; row++)
        {
            for (col = 0; col < COLS; col++)
            {
                heatmap_count_row[row][col] /= 2;
            }
        }
    }

    heatmap_count_row[row][col] += 1;

    if (heatmap_count_row[row][col] > heatmap_count_max)
    {
        heatmap_count_max = heatmap_count_row[row][col];
    }
}

static const char heatmap[] = ":=*%#";

const char *read_heatmap(int row_index) {
    strcpy(heatmap_str_row[0], "......   ......");
    strcpy(heatmap_str_row[1], "......   ......");
    strcpy(heatmap_str_row[2], "......   ......");
    strcpy(heatmap_str_row[3], "    ... ...    ");

    int row;
    int col;

    for (row = 0; row < ROWS; row++)
    {
        for (col = 0; col < COLS; col++)
        {
            if (heatmap_count_row[row][col])
            {
                heatmap_str_row[row][col] = heatmap[(heatmap_count_row[row][col] * ((sizeof(heatmap) / sizeof(heatmap[0])) - 2)) / heatmap_count_max];
            }
        }
    }

    convert_row_col(last_row, last_col, &row, &col);
    heatmap_str_row[row][col] = 'X';

    return heatmap_str_row[row_index];
}
